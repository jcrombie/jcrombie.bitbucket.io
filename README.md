# White-site generator #
This project builds a 'white-site' navigation based on the contents of a google spreadsheet.  It currently supports four levels of navigation, indicated by the first four columns of the spreadsheet.

The fifth column of the spreadsheet is for page content, which will be automatically updated as the user clicks around the navigation.

Live demo at:  https://white-site.aerobatic.io

## TODO:
1. Styling and accordion-animation on navigation elements, including set 'active' class on active link
2. Options for top/primary and secondary nav
3. Allow users to create and edit nav items, and save modifications back to the sheet

# [Start Bootstrap](http://startbootstrap.com/) - [Simple Sidebar](http://startbootstrap.com/template-overviews/simple-sidebar/)

[Simple Sidebar](http://startbootstrap.com/template-overviews/simple-sidebar/) is an off canvas sidebar navigation template for [Bootstrap](http://getbootstrap.com/) created by [Start Bootstrap](http://startbootstrap.com/).

## Creator

Start Bootstrap was created by and is maintained by **David Miller**, Managing Parter at [Iron Summit Media Strategies](http://www.ironsummitmedia.com/).

* https://twitter.com/davidmillerskt
* https://github.com/davidtmiller

Start Bootstrap is based on the [Bootstrap](http://getbootstrap.com/) framework created by [Mark Otto](https://twitter.com/mdo) and [Jacob Thorton](https://twitter.com/fat).

## Copyright and License

Copyright 2013-2015 Iron Summit Media Strategies, LLC. Code released under the [Apache 2.0](https://github.com/IronSummitMedia/startbootstrap-simple-sidebar/blob/gh-pages/LICENSE) license.